# Auth part for AlphaBrain

# Define Modal callback for login
loginCallback = (data) ->
  switch data.status
    when 'success'
      $('.navbar .navbar-right').empty().append(App.render_template('#tpl-loggedin-user', data.data))
      Modal.hide()