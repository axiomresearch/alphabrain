# Feeds handler
$('.subscription-container input#sub-query').on 'keypress', (e) ->
  if e.which is 13
    e.preventDefault()

    Request.postData '/api/feeds/subscription/', 
      {url: $(e.target).val()},
      (data) -> 
        console.log data