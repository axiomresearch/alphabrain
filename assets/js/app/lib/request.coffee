# Requests utility methods

### CSRF methods ###
csrfSafeMethod = (method) ->
  # these HTTP methods do not require CSRF protection
  (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method))

$.ajaxSetup(
  beforeSend: (xhr, settings) ->
    if not csrfSafeMethod(settings.type) and not @.crossDomain
        # Send the token to same-origin, relative URLs only.
        # Send the token only if the method warrants CSRF protection
        # Using the CSRFToken value acquired earlier
        xhr.setRequestHeader("X-CSRFToken", $.cookie('csrftoken'))
)

# HTTP methods
Request.getData = (url, callback) ->
  $.get url,
    (data) ->
      callback(data)

Request.postData = (url, dataToSend, callback) ->
  # Handle data type
  switch dataToSend 
    when typeof(dataToSend) is 'object'
      dataToSend = _.union(dataToSend, {csrfmiddlewaretoken: $.cookie('csrftoken')})
    when typeof(dataToSend) is 'string'
      dataToSend = dataToSend + '&csrfmiddlewaretoken=' + $.cookie('csrftoken')

  $.post url, 
  dataToSend,
  (data, textStatus, jqXHR) ->
    console.log jqXHR.getAllResponseHeaders()
    callback(data)