# Template utility methods
App.render_template = (template, data) ->
  tpl = Handlebars.compile $(template).html()
  tpl(data)