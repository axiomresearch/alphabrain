# 
# Modal utility methods
# #

# Utility methods to fill and display modal
# .........................................
show = () ->
  # Display modal
  $('#alphabrain-modal').modal 'show'

  # Set focus on first input field
  $('.modal input:first').focus()

Modal.hide = () ->
  # Hide modal
  $('#alphabrain-modal').modal 'hide'

###
This method allows to fill the modal content by loading it from remote content (GET url).
###
Modal.fillAndShowFromRemote = (url) ->
  $('.modal .modal-content').load url
  show()

###
This method render data into the specified template and show the modal.
###
Modal.renderAndShow = (template, data) ->
  $('.modal .modal-content').append App.render_template(template, data)
  show()

###
This higher-function render data into the specified template and show the modal.
###
renderAndShowHigher = (template) ->
  return (data) ->
    $('.modal .modal-content').append App.render_template(template, data)
    show()

###
This method allows to fill the modal content by loading data from server and render into the specified template.
###
Modal.fillAndShow = (url, template) ->
  Request.getData(url, renderAndShow(template))

# Utility methods to handle modal events
# ......................................
$('.modal').on 'hidden.bs.modal', (e) ->
  $(e.target).find('.modal-content').empty()

$('.modal').on 'submit', 'form', (e) ->
  if $(e.target).prop('id') is 'login' and App.AJAX_LOGIN
    e.preventDefault()
    
    url = $(e.target).data('url')
    Request.postData(url, $(e.target).serialize(), Modal.callback)