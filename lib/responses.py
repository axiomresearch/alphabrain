# -*- coding: utf-8 -*-

from django.http import HttpResponse
from simplejson import dumps

"""
Utility methods for Response
"""

def server_response(status, data=None, msg=None):
  # Response data
  content_type = 'application/json'
  content = {'status': None, 'msg': msg, 'data': data}

  if status == 200:
    content['status'] = 'success'
  elif status == 400:
    content['status'] = 'warning'
    content['msg'] = 'Bad data'
  elif status == 401:
    content['status'] = 'error'
    content['msg'] = 'Unauthorized request'
  elif status == 402:
    content['status'] = 'error'
    content['msg'] = 'Payment required'
  elif status == 403:
    content['status'] = 'error'
    content['msg'] = 'Not acceptable request'
  elif status == 404:
    content['status'] = 'warning'
    content['msg'] = 'No data found'
  elif status == 405:
    content['status'] = 'error'
    content['msg'] = 'Method not allowed'
  elif status == 406:
    content['status'] = 'error'
    content['msg'] = 'Not acceptable request'

  # Update response message if not empty
  if msg != None:
    content['msg'] = msg
    
  return HttpResponse(content=dumps(content), content_type=content_type, status=status)