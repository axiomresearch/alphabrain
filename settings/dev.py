# Development settings for AlphaBrain project.
from common import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
  'default': {
    'ENGINE': 'django.db.backends.sqlite3',
    'NAME': SITE_ROOT / 'db' / 'dev.db',
  }
}

#INSTALLED_APPS += (
# 'debug_toolbar',
#)

ALLOWED_HOSTS = []

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
