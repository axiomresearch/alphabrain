# -*- coding: utf-8 -*-

from django import forms

class SigninForm(forms.Form):
    username = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Your username', 'autocomplete': 'off'}))
    password = forms.CharField(required=True, max_length=20, widget=forms.PasswordInput(attrs={'placeholder': 'Your password', 'autocomplete': 'off'}))