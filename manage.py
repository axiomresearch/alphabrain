#!/usr/bin/env python
import os
import sys
import simplejson as json

if __name__ == "__main__":
  # Reading config file
  f = open('alphabrain.json', 'r')
  data = f.read()
  p_data = json.loads(data)

  # Set env variables
  #os.environ["DATABASE_URL"] = p_data["DATABASE_URL"]
  os.environ["SECRET_KEY"] = p_data["SECRET_KEY"]
  os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.dev")

  from django.core.management import execute_from_command_line

  execute_from_command_line(sys.argv)