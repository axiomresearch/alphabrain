from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
  # Feeds
  url(r'feeds/', include('alphabrain.apps.api.feeds.subscriptions.urls')),
)