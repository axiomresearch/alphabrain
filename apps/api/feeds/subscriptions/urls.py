from django.conf.urls import patterns, include, url
from views import subscription_view

urlpatterns = patterns('',
  # Feeds
  url(r'subscription/', subscription_view),
)