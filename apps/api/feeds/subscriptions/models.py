from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

"""
Subscriptions allow users to subscribe to all feeds they want and like.
"""
class Subscription(models.Model):
  subscriber = models.ForeignKey(User)
  title = models.CharField(max_length=255)
  url = models.CharField(max_length=255)
  updated = models.DateTimeField(auto_now=True, default=timezone.now)
  subtitle = models.TextField()
  language = models.CharField(max_length=3, default='en')
  related_group = models.CharField(max_length=40)


  class Meta:
    app_label = 'subscriptions'
    verbose_name = 'Subscription'
    verbose_name_plural = 'Subscriptions'

  def __unicode__(self):
    return "[%s](%s) - %s" % (self.subscriber, self.title, self.url)