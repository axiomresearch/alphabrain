from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from simplejson import loads, dumps, JSONDecodeError
from feedparser import parse
from dateutil.parser import parse as parsedate
from alphabrain.lib.responses import server_response
from .models import Subscription

def process_feed(user, feed):
  # Get data from feed
  title = feed.get('title', 'No title')
  subtitle = feed.get('subtitle', 'No subtitle')
  
  if 'links' in feed and len(feed.links) > 0:
    url = feed.links[0].href
  else:
    # Raise error
    pass

  if 'updated' in feed:
    updated = parsedate(feed.updated)
  elif 'published_parsed' in feed:
    updated = parsedate(feed.published_parsed)

  language = feed.get('language', 'en')

  # Create a new Subscription
  sub = Subscription(
    subscriber=user,
    title=title, url=url,
    updated=updated, subtitle=subtitle,
    language=language,
    related_group='no_group'
  )
  sub.save()

  # Send data in JSON to the client
  return dumps(sub)

@csrf_protect
@login_required
def subscription_view(request):
  if request.is_ajax():
 
     # Subscription HTTP POST method
    if request.method == 'POST' and request.POST:
      
      # Get data
      print request.POST
      raw_data = request.POST.get('url', None)

      if raw_data:
        try:
          data = loads(raw_data)

          if 'url' in data:
            # Try to parse the requested URL
            # Start with HTTP over SSL
            f = parse('https://' + data['url'])

            # Check parsed feed status
            if f.status in [200, 302]:
              feed = process_feed(request.user, f.feed)

              return server_response(data=feed, status=200) 

            elif f.status == 301:
              # Try to parse the requested URL
              f = parse('http://' + data['url'])

              if f.status in [200, 302]:
                feed = process_feed(request.user, f.feed)

                return server_response(data=feed, status=200) 

            return server_response(status=404, msg='Requested feed URL not found')

        except JSONDecodeError:
          return server_response(status=400)
      else:
        return server_response(status=400)

    else:
      return server_response(status=405)

  else:
    return server_response(status=406)