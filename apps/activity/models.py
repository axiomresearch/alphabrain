# -*- coding: utf-8 -*-

from django.db import models
from django.conf import settings
from django.utils import timezone

# --- ConnectionFailed ------------
class ConnectionFailed(models.Model):
	created_date = models.DateTimeField(auto_now_add=True, default=timezone.now)
	ip_address = models.CharField(max_length=15, default=u'0.0.0.0')
	username = models.CharField(max_length=30)
	password = models.CharField(max_length=20)

	def __unicode__(self):
		return "[%s - %s] : %s - %s" % (self.created_date, self.ip_address, self.username, self.password)

# --- Activity ------------
# Objects :
OBJECTS = (
	('USER', 'USER'),
	('SUBSCRIPTION', 'SUBSCRIPTION'),
	('SETTING', 'SETTING'),
)

# Tasks :
TASKS = (
	('LOGIN', 'LOGIN'),
	('LOGOUT', 'LOGOUT'),
	('NEW', 'NEW'),
	('UPDATE', 'UPDATE'),
	('DELETE', 'DELETE'),
	('CHANGE_PWD', 'CHANGE_PWD'),
	('SENDMAIL', 'SENDMAIL'),
	('SAVE', 'SAVE'),
)

# Status :
STATUS = (
	('SUCCESS', 'SUCCESS'),
	('ERROR', 'ERROR'),
	('WARNING', 'WARNING'),
)

class Activity(models.Model):
	"""
	This class defined an activity object which allows to track users activities. Each instance is reserved for a unique user. So it's very easy to have look on user actions.
	"""
	date = models.DateTimeField(auto_now_add=True, default=timezone.now)
	f_user = models.ForeignKey(settings.AUTH_USER_MODEL)
	f_object = models.CharField(max_length=8 ,choices=OBJECTS)
	f_task = models.CharField(max_length=10, choices=TASKS)
	f_status = models.CharField(max_length=8, choices=STATUS)
	target_id = models.IntegerField(null=True, blank=True) # Contains id of the targeted object.
	ip_address = models.IPAddressField(default=u'0.0.0.0')
	browser = models.CharField(max_length=30, null=True, blank=True)
	system = models.CharField(max_length=30, null=True, blank=True)
	content = models.CharField(max_length=100, null=True, blank=True)

	def __unicode__(self):
		return "[%s](%s|%s) : %s - %s - %s - %s" % (self.date, self.f_user, self.ip_address, self.f_object, self.f_task, self.f_status, self.target_id)
