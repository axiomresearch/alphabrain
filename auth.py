# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.contrib.auth import (authenticate, login, logout)
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from forms import SigninForm

from user_agents import parse

from apps.activity.models import (ConnectionFailed, Activity)

# Auth views
@csrf_protect
def signin_view(request):
  if request.method == 'POST' and request.POST:

    form = SigninForm(request.POST)
        
    if form.is_valid():

      # Validation
      cd = form.cleaned_data
      username = cd['username']
      password = cd['password']
            
      user = authenticate(username=username, password=password)
        
      if user is not None:
        # Keep trace about USER AGENT STRING
        ua = parse(request.META['HTTP_USER_AGENT'])
        browser = "%s/%s" % (ua.browser.family, ua.browser.version_string)
        system = "%s/%s" % (ua.os.family, ua.os.version_string)

        if user.is_active:
          login(request, user)

          a = Activity(f_user=user, f_object='USER', f_task='LOGIN', f_status='SUCCESS', ip_address=request.META['REMOTE_ADDR'], browser=browser, system=system)
          a.save()

          print(user.is_authenticated())

          return HttpResponseRedirect('/hello/')

        else:
          a = Activity(f_user=user, f_object='USER', f_task='LOGIN', f_status='ERROR', ip_address=request.META['REMOTE_ADDR'], browser=browser, system=system, content='User is not active')
          a.save()

          messages.error(request, 'Access denied')

      else:
        # Keep trace of activity
        cf = ConnectionFailed(ip_address=request.META['REMOTE_ADDR'], username=request.POST['username'], password=request.POST['password'])
        cf.save()

        messages.warning(request, 'Bad credentials')

    else:
      # Keep trace of activity
      cf = ConnectionFailed(ip_address=request.META['REMOTE_ADDR'], username=request.POST['username'], password=request.POST['password'])
      cf.save()

      messages.warning(request, 'Bad data')

  elif request.method == 'GET':
    form = SigninForm()

  return render_to_response('auth/signin.html', {'form': form}, context_instance=RequestContext(request))

@login_required
def logout_view(request):
  a = Activity(f_user=request.user, f_object='USER', f_task='LOGOUT', f_status='SUCCESS')
  a.save()
  
  logout(request)

  return HttpResponseRedirect('/')