###AlphaBrain

**AlphaBrain** is a News and Interests aggregator which allows people to bind all their feeds into one place, smartly. The application tracks user preferences and provides them pertinent contents based on these data. The system learns from user to give it more dedicated topics which could be interresting for him.

####TECHNOLOGIES

+ Django 1.6
+ Django-rest-framework
+ FeedParser.py
+ Angular.js