# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.contrib.auth.decorators import login_required

# Homepage view
def home_view(request):
  return render(request, 'home.html')

# Hello view
@login_required
def hello_view(request):
  return render(request, 'app/hello.html')