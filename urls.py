from django.conf.urls import patterns, include, url
from views import (home_view, hello_view)
from auth import (signin_view, logout_view)

urlpatterns = patterns('',
  # Homepage
  url(r'^$', home_view),

  # Main
  url(r'^hello/$', hello_view),

  # AI
  url(r'^ai/', include('alphabrain.apps.ai.urls')),

  # API
  url(r'^api/', include('alphabrain.apps.api.urls')),

  # Auth pages
  url(r'^signin/$', signin_view),
  url(r'^logout/$', logout_view),
)
